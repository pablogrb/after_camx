#!/bin/bash
rm class_uam_iv.mod
rm after_extract_stat
ifort class_uam_iv.f90 csv_file.f90 lcpgeo.f after_extract_stat.f90 -o after_extract_stat -O2 -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

# test_run-20130728.avrg
# SOAS-20130705.avrg

rm 13209-ACSM_LRK.csv
rm 13209-AMS_CTR.csv

./after_extract_stat << EOF
SOAS-20130705.avrg
after_CMUVBSS99.csv
LAMBERT
40. -97. 45. 33.
3
AMS_CTR    32.90     -87.25
ACSM_LRK   35.633433 -83.941507
AMS_RTP    35.889022 -78.874518
EOF
