#!/bin/bash
rm class_uam_iv.mod
rm libdate.mod
rm after_compare_network
ifort csv_file.f90 libdate.f90 class_uam_iv.f90 lcpgeo.f after_compare_network.f90 -o after_compare_network -g -traceback -check all -fp-stack-check -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

rm SOAS_IMPROVE.csv

./after_compare_network << EOF
Export_IMPROVE_SOAS_1l.csv
SOAS_IMPROVE.csv
-5
/Users2/pablogar/simout/12EUS/pgi143D_GMT/ALL_1Z/SOAS-
.avrg
after_CMUVBSS99_PM25.csv
LAMBERT
40. -97. 45. 33.
EOF

# ./after_compare_network << EOF
# Export_IMPROVE_SOAS.csv
# SOAS_IMPROVE.csv
# 0
# /Users2/pablogar/simout/12EUS/pgi143D_GMT/ALL/SOAS-
# .avrg
# after_CMUVBSS99_PM25.csv
# LAMBERT
# 40. -97. 45. 33.
# EOF

# ./after_compare_network << EOF
# /Users2/pablogar/simout/12EUS/pgi143D_150415/SOAS_EPA-CSN/Export_EPA-CSN_SOAS.csv
# SOAS_EPA-CSN.csv
# /Users2/pablogar/simout/12EUS/pgi143D_150415/ALL/SOAS-
# .avrg
# 0
# after_CMUVBSS99_PM25.csv
# LAMBERT
# 40. -97. 45. 33.
# EOF
