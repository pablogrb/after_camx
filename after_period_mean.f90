PROGRAM after_period_mean

USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Combines several PMCAMx08 AVERAGE files into a single avrg file with the mean
!		concentration for the entire period
!		Applies a species conversion matrix to process output
!	Inputs:
!		UAM-IV AVERAGE type files of PMCAMx08 with the CMU-VBS-S99 configuration of B. Murphy
!		and M. Day
!	Outputs:
!		Single AVERAGE file with one time record of the mean concentration for the period after
!		processing the concentrations with a conversion matrix
!	By:
!		Pablo Garcia
!		10-2016
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

! 	Data type modules
	TYPE(UAM_IV) :: fl_inp							! Current input average file
	TYPE(UAM_IV) :: fl_out							! Output average file

!	Conversion array
	CHARACTER(LEN=265) :: mat_file					! Matrix file name
	INTEGER :: n_out_spec							! Number of output species
	CHARACTER(LEN=10), ALLOCATABLE :: s_inp_spec(:)	! Species array of the input UAM_IV file
	CHARACTER(LEN=10), ALLOCATABLE :: s_out_spec(:)	! Species array of the output

	REAL, ALLOCATABLE :: conv_matrix(:,:)			! Linear transformation matrix for species
	REAL, ALLOCATABLE :: out_conc(:)				! Dummy output concentration vector

!	File averaging
	INTEGER :: n_files								! Number of files to process
	INTEGER :: n_frames								! Total number of frames in the combined files

!	Counters
	INTEGER :: i_sp_o, i_sp_i
	INTEGER :: i_fl
	INTEGER :: i_nx, i_ny
	INTEGER :: i
	INTEGER :: pct_10								! 10% counter

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
!	Get the output file name
	WRITE(*,*) 'Output file name'
	READ (*,'(a)') fl_out%in_file
	WRITE(*,'(a)') TRIM(fl_out%in_file)
! 	Set the output file unit
	fl_out%unit = 11

!	------------------------------------------------------------------------------------------
!	Read the species matrix part 1
!
!	Get the matrix file name
	WRITE(*,*) 'Linear transformation matrix file name: '
	READ (*,'(a)') mat_file
	WRITE(*,'(a)') TRIM(mat_file)
!	Open the file
	OPEN(UNIT=21,FILE=TRIM(mat_file),STATUS='OLD')
!	Read the number of out species.
	READ (21,*) n_out_spec
	WRITE(*,*) 'Number of output species:', n_out_spec
	REWIND 21

!	------------------------------------------------------------------------------------------
!	Process the files
!	------------------------------------------------------------------------------------------
!
!	Set the input file unit
	fl_inp%unit = 31
!	Get the number of files to process
	WRITE(*,*) 'Number of files to process'
	READ (*,*) n_files
	WRITE(*,*) n_files

!	Get the first file name
	WRITE(*,*) 'Input file name'
	READ (*,'(a)') fl_inp%in_file
	WRITE(*,'(a)') TRIM(fl_inp%in_file)
!	Read the file
	CALL read_uamfile(fl_inp)

!	------------------------------------------------------------------------------------------
!	Build the output file header using the header of the first file and the user dates
	fl_out%ftype  = fl_inp%ftype
	fl_out%update_times = 1
! 	Header 1
	fl_out%fname  = fl_inp%fname
	fl_out%note   = fl_inp%note
	fl_out%nseg   = fl_inp%nseg
	fl_out%nspec  = n_out_spec
	fl_out%idate  = fl_inp%idate
	fl_out%begtim = fl_inp%begtim
! 	fl_out%jdate  = fl_inp%jdate
! 	fl_out%endtim = fl_inp%endtim
! 	Header 2
	fl_out%orgx = fl_inp%orgx
	fl_out%orgy = fl_inp%orgy
	fl_out%iutm = fl_inp%iutm
	fl_out%utmx = fl_inp%utmx
	fl_out%utmy = fl_inp%utmy
	fl_out%dx   = fl_inp%dx
	fl_out%dy   = fl_inp%dy
	fl_out%nx   = fl_inp%nx
	fl_out%ny   = fl_inp%ny
	fl_out%nz   = 1
	fl_out%nzlo = fl_inp%nzlo
	fl_out%nzup = fl_inp%nzup
	fl_out%hts  = fl_inp%hts
	fl_out%htl  = fl_inp%htl
	fl_out%htu  = fl_inp%htu
! 	Header 3
	fl_out%i1  = fl_inp%i1
	fl_out%j1  = fl_inp%j1
	fl_out%nx1 = fl_inp%nx1
	fl_out%ny1 = fl_inp%ny1

! 	Allocate the time headers
	ALLOCATE(fl_out%ibgdat(1),fl_out%iendat(1))
	ALLOCATE(fl_out%nbgtim(1),fl_out%nentim(1))
	fl_out%ibgdat = fl_inp%idate
! 	fl_out%iendat = fl_inp%begtim
	fl_out%nbgtim = fl_inp%jdate
! 	fl_out%nentim = fl_inp%endtim

! 	Allocate the output concentration array with 1 z level, 1 frame and the output species
	ALLOCATE(fl_out%conc(fl_inp%nx,fl_inp%ny,1,1,n_out_spec))
! 	Initialize the array
	fl_out%conc(:,:,:,:,:) = 0

!	------------------------------------------------------------------------------------------
!	Read the species matrix part 2
!
! 	Allocate the vectors and arrays
	ALLOCATE(s_inp_spec(fl_inp%nspec))
	ALLOCATE(s_out_spec(n_out_spec))
	ALLOCATE(conv_matrix(fl_inp%nspec,n_out_spec))
! 	Read the out species list
	READ (21,*) n_out_spec, (s_out_spec(i_sp_o), i_sp_o=1,n_out_spec)
! 	Read the matrix
	DO i_sp_i = 1, fl_inp%nspec
		READ (21,*) s_inp_spec(i_sp_i), (conv_matrix(i_sp_i,i_sp_o), i_sp_o=1,n_out_spec)
	END DO

! 	Allocate the ouput concentration vector
	ALLOCATE(out_conc(n_out_spec))

!	------------------------------------------------------------------------------------------
!	Build the species list
	ALLOCATE(fl_out%spname(10,n_out_spec))
	ALLOCATE(fl_out%c_spname(n_out_spec))
	DO i_sp_o = 1, n_out_spec
		DO i = 1,10
			fl_out%spname(i,i_sp_o) = s_out_spec(i_sp_o)(i:i)
		END DO
	END DO
	fl_out%c_spname = s_out_spec

!	------------------------------------------------------------------------------------------
!	Process the files
!	------------------------------------------------------------------------------------------
!
!	Initialize the frame counter
	n_frames = 0

!	Array operation testing
! 	WRITE(*,*) 'SHAPE of SUM along DIM=1'
! 	WRITE(*,*) SHAPE(SUM(fl_inp%conc(i_nx,i_ny,1,:,:),DIM=1))
! 	WRITE(*,*) 'SHAPE of SUM along DIM=2'
! 	WRITE(*,*) SHAPE(SUM(fl_inp%conc(i_nx,i_ny,1,:,:),DIM=2))

!	Loop through the files
	DO i_fl = 1, n_files
		WRITE(*,*) 'Working on', i_fl, 'of', n_files,'files'
		IF (i_fl .GT. 1) THEN
!	 		Get the file name
			WRITE(*,*) 'Input file name'
			READ (*,'(a)') fl_inp%in_file
			WRITE(*,'(a)') TRIM(fl_inp%in_file)
!	 		Read the file
			CALL read_uamfile(fl_inp)
		END IF

! 		Update the frame counter
		n_frames = n_frames + fl_inp%update_times

! 		Loop through the rows and columns
		WRITE(*,*) 'Converting the species'
		pct_10 = 1+fl_inp%nx/10
		WRITE(*,*) 0, "% done"
		DO i_nx=1,fl_inp%nx
			DO i_ny=1,fl_inp%ny
				fl_out%conc(i_nx,i_ny,1,1,:) = &
					& MATMUL(SUM(fl_inp%conc(i_nx,i_ny,1,:,:),DIM=1),conv_matrix)
			END DO
! 			Sanity display
			IF (MOD(i_nx,pct_10) .EQ. 0) THEN
				WRITE(*,*) 100*i_nx/fl_inp%nx, "% done"
			END IF
		END DO
		WRITE(*,*) 100, "% done"

	END DO

! 	Calculate the mean
! 	Loop through the rows and columns
	pct_10 = 1+fl_inp%nx/10
	WRITE(*,*) 'Averaging frames', n_frames
	DO i_nx=1,fl_inp%nx
		DO i_ny=1,fl_inp%ny
			fl_out%conc(i_nx,i_ny,1,1,:) = fl_out%conc(i_nx,i_ny,1,1,:)/REAL(n_frames)
		END DO
!		Sanity display
		IF (MOD(i_nx,pct_10) .EQ. 0) THEN
			WRITE(*,*) 100*i_nx/fl_inp%nx, "% done"
		END IF
	END DO
	WRITE(*,*) 100, "% done"

!	Get the end times
	fl_out%jdate  = fl_inp%jdate
	fl_out%endtim = fl_inp%endtim
	fl_out%iendat = fl_inp%jdate
	fl_out%nentim = fl_inp%endtim
! 	Write the output file
	CALL write_uamfile(fl_out)

END PROGRAM after_period_mean
