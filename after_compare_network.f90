PROGRAM after_compare_network

USE csv_file
USE LibDate
USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Extracts daily averaged station data from PMCAMx08 avrg files
!		Applies a species conversion matrix to process output
!		Collates the output with a network station data file
!	Inputs:
!		Network station data file created from the FED database and formated using an Excel
!			template
!		Conversion matrix file
!		UAM-IV AVERAGE type files of PMCAMx08 with the CMU-VBS-S99 configuration of B. Murphy
!			and M. Day
!	Outputs:
!		Collated daily average data in a csv file
!	By:
!		Pablo Garcia
!		11-2016
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

! 	Data type module
	TYPE(UAM_IV) :: fl(2)							! Input average files

!	Network data
	CHARACTER(LEN=265) :: ntw_file					! Network data file name
	CHARACTER(LEN=5) :: ntw_code					! Current station site code
	CHARACTER(LEN=8), DIMENSION(2) :: ntw_date		! Current station date
	INTEGER, DIMENSION(3) :: ntw_base_date			! Current station base date array
	INTEGER :: ntw_tz								! Current station time zone
	REAL :: ntw_OMOC								! Current station OM/OC ratio
	REAL :: ntw_lat, ntw_lon						! Current station latitude and longitude
	REAL :: ntw_xkm, ntw_ykm						! Current station projection coordinates
	INTEGER :: ntw_x, ntw_y 						! Current station cell
	REAL :: ntw_val(7)								! Current station value vector

! 	Simulation time zone
	INTEGER :: fl_tz
	INTEGER :: delta_tz

!	Date variables
	TYPE(DateType) :: fl_date(2)					! Dates of the input average files

!	Output
	CHARACTER(LEN=256) :: out_file					! Name of the output file

! 	Input file
	CHARACTER(LEN=256) :: inp_file_r, inp_file_l 	! Right and left sides of the input file name
	CHARACTER(LEN=256) :: inp_file(2)				! Input file name
	! INTEGER :: inp_date_type						! 0 = YYYYMMDD, 1=DDD
	INTEGER :: inp_date_len	= 8						! 8 = YYYYMMDD, 3=DDD
	CHARACTER(LEN=8), DIMENSION(2) :: inp_date		! Current input file date

!	Conversion array
	CHARACTER(LEN=265) :: mat_file					! Matrix file name
	INTEGER :: n_out_spec							! Number of output species
	CHARACTER(LEN=10), ALLOCATABLE :: s_inp_spec(:)	! Species array of the input UAM_IV file
	CHARACTER(LEN=10), ALLOCATABLE :: s_out_spec(:)	! Species array of the output

	REAL, ALLOCATABLE :: conv_matrix(:,:)			! Linear transformation matrix for species
	REAL, ALLOCATABLE :: out_conc(:)				! Dummy output concentration vector

!	Projection parameters
	CHARACTER(LEN=10) :: projection					! Projection type
! 	LAMBERT
	REAL :: phic									! Central latitude (deg, neg for southern hem)
	REAL :: xlonc									! Central longitude (deg, neg for western hem)
	REAL :: truelat1								! First true latitute (deg, neg for southern hem)
	REAL :: truelat2								! Second true latitute (deg, neg for southern hem)

!	Station parameters
! 	INTEGER :: nstat								! Number of stations
! 	CHARACTER(LEN=10), ALLOCATABLE :: stat_code(:)	! Station code name
! 	REAL, ALLOCATABLE :: stat_lat(:), stat_lon(:)	! Station latitude and longitude
! 	REAL :: stn_xkm, stn_ykm						! Current station lambert projection coordinate (km)
! 	INTEGER :: stn_x, stn_y							! Current station model cell coordinate

!	Counters
	INTEGER :: i_sp_o, i_sp_i, i_sp_n
	INTEGER :: i_date
! 	INTEGER :: i_st
! 	INTEGER :: i_hr

!	Read control
	INTEGER :: io_status

!	Logicals
	LOGICAL :: file_exists
	LOGICAL :: csv_record_end

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
! 	Get the network data file
	WRITE(*,*) 'Network data file'
	READ (*,'(a)') ntw_file
	WRITE(*,*) TRIM(ntw_file)
! 	Open the file
	OPEN(UNIT=11,FILE=TRIM(ntw_file),FORM='FORMATTED',STATUS='OLD')

!	Get the ouput data path
	WRITE(*,*) 'Output file name'
	READ (*,'(a)') out_file
	WRITE(*,*) TRIM(out_file)
! 	Open the file
	OPEN(UNIT=21,FILE=TRIM(out_file),FORM='FORMATTED',STATUS='NEW')

! 	Get the simulation time zone
	WRITE(*,*) 'Simulation time zone'
	READ (*,*) fl_tz
	WRITE(*,*) fl_tz

! 	Get the two sides of the input file
	WRITE(*,*) 'Input files are processed as left side // date // right side'
	WRITE(*,*) 'Left hand side of the input file path'
	READ (*,'(a)') inp_file_l
	WRITE(*,*) TRIM(inp_file_l)
	WRITE(*,*) 'Right hand side of the input file path'
	READ (*,'(a)') inp_file_r
	WRITE(*,*) TRIM(inp_file_r)

! 	Test if the path is too long
	IF (LEN(TRIM(inp_file_l))+LEN(TRIM(inp_file_r))+inp_date_len .GT. 256 ) THEN
		WRITE(*,*) 'Input file path too long, try linking or relative paths'
		STOP
	END IF

!	Get the matrix file name
	WRITE(*,*) 'Linear transformation matrix file name: '
	READ (*,'(a)') mat_file
	WRITE(*,'(a)') TRIM(mat_file)
!	Open the file
	OPEN(UNIT=31,FILE=TRIM(mat_file),STATUS='OLD')

!	Get the projection parameters
	WRITE(*,*) 'Projection Type:'
	READ (*,'(a)') projection
	WRITE (*,*) projection

	SELECT CASE (projection)
		CASE ('LAMBERT')
			WRITE(*,*) 'Projection parameters:'
			WRITE(*,*) 'Central lat, central lon, True lat 1, True lat 2'
			READ (*,*) phic, xlonc, truelat1, truelat2
			WRITE(*,*) phic, xlonc, truelat1, truelat2

		CASE DEFAULT
			WRITE(*,*) 'Projection type not supported'
	END SELECT

!	------------------------------------------------------------------------------------------
!	Set up files and conversion matrix
!	------------------------------------------------------------------------------------------
!
! 	Skip the header line of the network data file
	READ (11,*)
!	Read the first data line of the network file
	READ (11,*,IOSTAT=io_status) ntw_code, ntw_date(1), ntw_tz, ntw_OMOC, ntw_lat, ntw_lon, ntw_val

! 	Get the base date components
	READ(ntw_date(1)(1:4),*) ntw_base_date(1)
	READ(ntw_date(1)(5:6),*) ntw_base_date(2)
	READ(ntw_date(1)(7:8),*) ntw_base_date(3)

!	Set the base date variable
	fl_date(1) = DateType(ntw_base_date(1),ntw_base_date(2),ntw_base_date(3),0,0)
! 	Get the secondary date
	delta_tz = ntw_tz-fl_tz
	fl_date(2) = fl_date(1) + DateType(0,0,0,delta_tz,0)
	CALL format_date(fl_date(2),'yyyymmdd',ntw_date(2))

! 	Build the first file names
	inp_file(1) = TRIM(inp_file_l) // ntw_date(1) // TRIM(inp_file_r)
	inp_file(2) = TRIM(inp_file_l) // ntw_date(2) // TRIM(inp_file_r)

! 	Test if the files exists
	DO i_date = 1,2
		INQUIRE(FILE=TRIM(inp_file(i_date)), EXIST=file_exists)
		IF ( .NOT. file_exists) THEN
			WRITE(*,*) 'The input file does not exist'
			WRITE(*,*) inp_file(i_date)
			STOP
		END IF
	END DO

! ! ! 	Set up the UAM-IV type
! 	fl(1)%in_file = inp_file(1)
! 	fl(1)%unit = 41
! ! 	Open the file read the headers, read the species and close the file
! !	All procedures from the UAM-IV class
! 	CALL read_open_file(fl(1))
! 	CALL read_header(fl(1))
! 	CALL read_species(fl(1))
! 	CALL close_file(fl(1))
	CALL read_uamfile(fl(1),inp_file(1))

!	Get the conversion matrix
!	Read the number of out species.
	READ (31,*) n_out_spec
	WRITE(*,*) 'Number of output species:', n_out_spec
	REWIND 31
! 	Allocate the vectors and arrays
	ALLOCATE(s_inp_spec(fl(1)%nspec))
	ALLOCATE(s_out_spec(n_out_spec))
	ALLOCATE(conv_matrix(fl(1)%nspec,n_out_spec))
! 	Read the out species list
	READ (31,*) n_out_spec, (s_out_spec(i_sp_o), i_sp_o=1,n_out_spec)
! 	Read the matrix
	DO i_sp_i = 1, fl(1)%nspec
		READ (31,*) s_inp_spec(i_sp_i), (conv_matrix(i_sp_i,i_sp_o), i_sp_o=1,n_out_spec)
	END DO
! 	Allocate the ouput concentration vector
	ALLOCATE(out_conc(n_out_spec))

!	Write the output file headers
	CALL csv_write(21,'Site_Code',.FALSE.)
	CALL csv_write(21,'Date',.FALSE.)
	CALL csv_write(21,'OM_OC',.FALSE.)
	CALL csv_write(21,'Lat',.FALSE.)
	CALL csv_write(21,'Lon',.FALSE.)
	CALL csv_write(21,'xkm',.FALSE.)
	CALL csv_write(21,'ykm',.FALSE.)
	CALL csv_write(21,'x',.FALSE.)
	CALL csv_write(21,'y',.FALSE.)
	CALL csv_write(21,'ntw_PSO4',.FALSE.)
	CALL csv_write(21,'ntw_PNO3',.FALSE.)
	CALL csv_write(21,'ntw_PNH4',.FALSE.)
	CALL csv_write(21,'ntw_OC',.FALSE.)
	CALL csv_write(21,'ntw_OM',.FALSE.)
	CALL csv_write(21,'ntw_EC',.FALSE.)
	CALL csv_write(21,'ntw_PM25',.FALSE.)
! 	species
	DO i_sp_o=1,n_out_spec
		IF (i_sp_o .LT. n_out_spec) THEN
			csv_record_end=.FALSE.
		ELSE
			csv_record_end=.TRUE.
		END IF
		CALL csv_write (21,s_out_spec(i_sp_o),csv_record_end)
	END DO

!	Rewind the network data file to start the reading loop
	REWIND 11
! 	Skip the headers again
	READ (11,*)

!	------------------------------------------------------------------------------------------
!	Loop through the network sites
!	------------------------------------------------------------------------------------------
!
!	Set the current input file dates to far, far away (change if needed)
	! inp_date(1)='19000101'
	inp_date(1)=ntw_date(1) ! The first file is already open
	inp_date(2)='19000101'
! 	Read through the network file until EOF
	DO
! 		Read the network file
		READ (11,*,IOSTAT=io_status) ntw_code, ntw_date(1), ntw_tz, ntw_OMOC, ntw_lat, ntw_lon, ntw_val
! 		Check for errors
		IF (io_status > 0) THEN
			WRITE(*,*) 'Error reading station file'
			STOP
! 		Check for EOF
		ELSE IF (io_status < 0) THEN
			WRITE(*,*) 'Reached end of file'
			STOP
! 		Everything OK! Proceed
		ELSE

! 		Get the base date components
		READ(ntw_date(1)(1:4),*) ntw_base_date(1)
		READ(ntw_date(1)(5:6),*) ntw_base_date(2)
		READ(ntw_date(1)(7:8),*) ntw_base_date(3)

!		Set the base date variable
		fl_date(1) = DateType(ntw_base_date(1),ntw_base_date(2),ntw_base_date(3),0,0)
! 		Get the secondary date
		fl_date(2) = fl_date(1) + DateType(0,0,0,delta_tz,0)
		CALL format_date(fl_date(2),'yyyymmdd',ntw_date(2))

!		------------------------------------------------------------------------------------------
! 			Get the cell number
			CALL lcpgeo(0,phic,xlonc,truelat1,truelat2,ntw_xkm,ntw_ykm,ntw_lon,ntw_lat)
! 			Calculate the cell position
			ntw_x = INT((ntw_xkm - fl(1)%utmx/1000)/(fl(1)%dx/1000))+1
			ntw_y = INT((ntw_ykm - fl(1)%utmy/1000)/(fl(1)%dy/1000))+1
!	 		Check if in grid
			IF (ntw_x > 0 .AND. ntw_x <= fl(1)%nx .AND. ntw_y > 0 .AND. ntw_y <= fl(1)%ny) THEN
! 				Sanity Output
				WRITE(*,*) 'Working on ', ntw_code, ' ',  ntw_date(1)
				DO i_date = 1,2
	! 				Check if the UAM-IV files are not the same
					IF (.NOT. (ntw_date(i_date) .EQ. inp_date(i_date))) THEN
	! 					Update the date
						inp_date(i_date) = ntw_date(i_date)
	! 					Build the file name
						inp_file(i_date) = TRIM(inp_file_l) // ntw_date(i_date) // TRIM(inp_file_r)
	! 					Test if the file exists
						INQUIRE(FILE=TRIM(inp_file(i_date)), EXIST=file_exists)
						IF ( .NOT. file_exists) THEN
							WRITE(*,*) 'The input file does not exist'
							STOP
						ELSE
	! 						Open the file
							fl(i_date)%in_file = inp_file(i_date)
							CALL read_uamfile(fl(i_date))
						END IF
					END IF
				END DO
!		------------------------------------------------------------------------------------------
! 				Copy the network data with added position info
				CALL csv_write(21,ntw_code,.FALSE.)
				CALL csv_write(21,ntw_date(1),.FALSE.)
				CALL csv_write(21,ntw_OMOC,.FALSE.)
				CALL csv_write(21,ntw_lat,.FALSE.)
				CALL csv_write(21,ntw_lon,.FALSE.)
				CALL csv_write(21,ntw_xkm,.FALSE.)
				CALL csv_write(21,ntw_ykm,.FALSE.)
				CALL csv_write(21,ntw_x,.FALSE.)
				CALL csv_write(21,ntw_y,.FALSE.)
! 				Copy the network concentrations
				DO i_sp_n = 1,7
					CALL csv_write(21,ntw_val(i_sp_n),.FALSE.)
				END DO
! 				Calculate the output species vector
				IF (delta_tz .EQ. 0) THEN
					WRITE(*,*) "delta_tz == 0"
					out_conc = MATMUL(SUM(fl(1)%conc(ntw_x,ntw_y,1,:,:),DIM=1),&
									 &conv_matrix)/fl(1)%update_times
				ELSE IF (delta_tz .LT. 0) THEN
					out_conc = MATMUL(SUM(fl(2)%conc(ntw_x,ntw_y,1,25+delta_tz:24,:),DIM=1)+&
									 &SUM(fl(1)%conc(ntw_x,ntw_y,1,1:24+delta_tz,:),DIM=1),&
									 &conv_matrix)/fl(1)%update_times
				ELSE IF (delta_tz .GT. 0) THEN
					WRITE(*,*) "delta_tz > 0"
					out_conc = MATMUL(SUM(fl(1)%conc(ntw_x,ntw_y,1,25-delta_tz:24,:),DIM=1)+&
									 &SUM(fl(2)%conc(ntw_x,ntw_y,1,1:24-delta_tz,:),DIM=1),&
									 &conv_matrix)/fl(1)%update_times
				END IF
! 				Write the output concentrations
				DO i_sp_o = 1,n_out_spec
					IF (i_sp_o .LT. n_out_spec) THEN
						csv_record_end=.FALSE.
					ELSE
						csv_record_end=.TRUE.
					END IF
					CALL csv_write (21,out_conc(i_sp_o),csv_record_end)
				END DO
			END IF
		END IF
	END DO

END PROGRAM  after_compare_network
