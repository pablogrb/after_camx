# Base Path
BASE_PATH=/Users2/pablogar/simout/36EUS/2001_base/
# Input file
in_file=${BASE_PATH}2001_base-${tvdate[4]}${tvdate[1]}${tvdate[2]}.avrg
# Conversion Matrix file
mat_file=after_CMUVBSS99_PM25.csv
# Build the control file
# Contains:
# 1. PMCAMx .avrg file for input
# 2. Species conversion matrix file
#		First row contains the no. of out species and the out species names (len=10)
#		There should be a row for every species in the input file
#		Each of this rows starts with the species name (len=10) and contains the species coefficents
#		Species are transformed by a matrix vector operation Ax=b
#		Where A is the conversion matrix, x is the input species vector and b is the output species vector
# 3. Projection, for now only LAMBERT is supported
# 4. Projection parameters
#		LAMBERT: central lat, central lon, true lat 1, true lat 2
# 5. No. of stations to process
# 6. Station records
#		There should be as many station records as specified on the previous line
#		Each record contains (in space separated columns)
#		Station code (len=10), Latitude (decimal degrees), Longitude (decimal degrees)
cat << EOF > after.in
$in_file
$mat_file
LAMBERT
40. -97. 45. 33.
1
CMU_base  40.442602 -79.943318
EOF
