#!/bin/bash
rm class_uam_iv.mod
rm after_period_conc
#ifort class_uam_iv.f90 after_period_conc.f90 -o after_period_conc -O2 -openmp -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source
ifort class_uam_iv.f90 after_period_conc.f90 -o after_period_conc -g -mieee-fp -align dcommons -convert big_endian -static_intel -extend-source

# rm SOAS-mean.avrg

rm 2001_base-mean_PM25_1day.avrg
BASE_PATH=/Users2/pablogar/simout/36EUS/2001_base/
./after_period_conc << EOF
2001_base-mean_PM25_1day.avrg
after_CMUVBSS99_PM25.csv
1
${BASE_PATH}2001_base-20010714.avrg
EOF

# ./after_period_mean << EOF
# SOAS-mean.avrg
# after_CMUVBSS99.csv
# 2
# SOAS-20130705.avrg
# SOAS-20130706.avrg
# EOF

# ./after_period_mean << EOF
# SOAS-mean.avrg
# after_CMUVBSS99_PM25.csv
# 1
# SOAS-20130628.avrg
# EOF
