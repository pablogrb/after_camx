PROGRAM after_extract_stat

USE csv_file
USE class_uam_iv

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Extracts hourly station data from PMCAMx08 avrg files
!		Applies a species conversion matrix to process output
!	Inputs:
!		UAM-IV AVERAGE type file of PMCAMx08 with the CMU-VBS-S99 configuration of B. Murphy
!		and M. Day
!	Outputs:
!		Hourly data per station in csv files
!	By:
!		Pablo Garcia
!		08-2016
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

! 	Data type module
	TYPE(UAM_IV) :: fl								! Input average file

!	Conversion array
	CHARACTER(LEN=265) :: mat_file					! Matrix file name
	INTEGER :: n_out_spec							! Number of output species
	CHARACTER(LEN=10), ALLOCATABLE :: s_inp_spec(:)	! Species array of the input UAM_IV file
	CHARACTER(LEN=10), ALLOCATABLE :: s_out_spec(:)	! Species array of the output

	REAL, ALLOCATABLE :: conv_matrix(:,:)			! Linear transformation matrix for species
	REAL, ALLOCATABLE :: out_conc(:)				! Dummy output concentration vector

!	Projection parameters
	CHARACTER(LEN=10) :: projection					! Projection type
! 	LAMBERT
	REAL :: phic									! Central latitude (deg, neg for southern hem)
	REAL :: xlonc									! Central longitude (deg, neg for western hem)
	REAL :: truelat1								! First true latitute (deg, neg for southern hem)
	REAL :: truelat2								! Second true latitute (deg, neg for southern hem)

!	Station parameters
	INTEGER :: nstat								! Number of stations
	CHARACTER(LEN=10), ALLOCATABLE :: stat_code(:)	! Station code name
	REAL, ALLOCATABLE :: stat_lat(:), stat_lon(:)	! Station latitude and longitude
	REAL :: stn_xkm, stn_ykm						! Current station lambert projection coordinate (km)
	INTEGER :: stn_x, stn_y							! Current station model cell coordinate

!	Output
	CHARACTER(LEN=6) :: str_idate
	CHARACTER(LEN=256) :: out_file					! Name of the output file

!	Counters
	INTEGER :: i_sp_o, i_sp_i
	INTEGER :: i_st
	INTEGER :: i_hr

!	Logicals
	LOGICAL :: csv_record_end

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	User IO
!
! 	Get the input file name
	WRITE(*,*) 'Input file name: '
	READ (*,'(a)') fl%in_file
	WRITE(*,'(a)') TRIM(fl%in_file)
! 	Set the input file unit
	fl%unit = 21
! 	Read the input file
	CALL read_uamfile(fl)

!	------------------------------------------------------------------------------------------
!	Read the species matrix
!
!	Get the matrix file name
	WRITE(*,*) 'Linear transformation matrix file name: '
	READ (*,'(a)') mat_file
	WRITE(*,'(a)') TRIM(mat_file)
!	Open the file
	OPEN(UNIT=31,FILE=TRIM(mat_file),STATUS='OLD')
!	Read the number of out species.
	READ (31,*) n_out_spec
	WRITE(*,*) 'Number of output species:', n_out_spec
	REWIND 31
! 	Allocate the vectors and arrays
	ALLOCATE(s_inp_spec(fl%nspec))
	ALLOCATE(s_out_spec(n_out_spec))
	ALLOCATE(conv_matrix(fl%nspec,n_out_spec))
! 	Read the out species list
	READ (31,*) n_out_spec, (s_out_spec(i_sp_o), i_sp_o=1,n_out_spec)
! 	Read the matrix
	DO i_sp_i = 1, fl%nspec
		READ (31,*) s_inp_spec(i_sp_i), (conv_matrix(i_sp_i,i_sp_o), i_sp_o=1,n_out_spec)
	END DO

! 	Allocate the ouput concentration vector
	ALLOCATE(out_conc(n_out_spec))

!	------------------------------------------------------------------------------------------
!	Read the projection parameters
!
	WRITE(*,*) 'Projection Type:'
	READ (*,'(a)') projection
	WRITE (*,*) projection

	SELECT CASE (projection)
		CASE ('LAMBERT')
			WRITE(*,*) 'Projection parameters:'
			WRITE(*,*) 'Central lat, central lon, True lat 1, True lat 2'
			READ (*,*) phic, xlonc, truelat1, truelat2
			WRITE(*,*) phic, xlonc, truelat1, truelat2

		CASE DEFAULT
			WRITE(*,*) 'Projection type not supported'
	END SELECT

!	------------------------------------------------------------------------------------------
!	Read the station data
!
	WRITE(*,*) 'Number of station records:'
	READ (*,*) nstat
	WRITE(*,*) nstat

! 	Allocate the station parameter arrays
	ALLOCATE(stat_code(nstat),stat_lat(nstat),stat_lon(nstat))

! 	Loop through station data
	DO i_st = 1,nstat
		READ (*,*) stat_code(i_st), stat_lat(i_st), stat_lon(i_st)
		WRITE(*,*) stat_code(i_st), stat_lat(i_st), stat_lon(i_st)
	END DO

!	------------------------------------------------------------------------------------------
!	Get the station data from the UAM_IV file
!
! 	Loop through the stations
	DO i_st = 1,nstat
!	------------------------------------------------------------------------------------------
! 		Get the station cell coordinate
		CALL lcpgeo(0,phic,xlonc,truelat1,truelat2,stn_xkm,stn_ykm,stat_lon(i_st),stat_lat(i_st))
! 		Calculate the cell position
		stn_x = INT((stn_xkm-fl%utmx/1000)/(fl%dx/1000))+1
		stn_y = INT((stn_ykm-fl%utmy/1000)/(fl%dy/1000))+1
!	 	Check if in grid
		IF (.NOT. (stn_x > 0 .AND. stn_x <= fl%nx .AND. stn_y > 0 .AND. stn_y <= fl%ny)) THEN
			WRITE(*,*) 'The location is not inside the modeling grid'
			STOP
		END IF

!	------------------------------------------------------------------------------------------
! 		Get the date from the UAM_IV file
		WRITE(str_idate,'(i6)') fl%idate
! 		Build the file name
		out_file = TRIM(ADJUSTL(str_idate)) // '-' // TRIM(stat_code(i_st)) // '.csv'
		WRITE(*,*) TRIM(out_file)
! 		Open the file
		OPEN(UNIT=41,FILE=TRIM(out_file),FORM='FORMATTED',STATUS='NEW')

!		Write the CSV header
! 		date and time
		CALL csv_write (41,'date',.FALSE.)
		CALL csv_write (41,'time',.FALSE.)
! 		species
		DO i_sp_o=1,n_out_spec
			IF (i_sp_o .LT. n_out_spec) THEN
				csv_record_end=.FALSE.
			ELSE
				csv_record_end=.TRUE.
			END IF
			CALL csv_write (41,s_out_spec(i_sp_o),csv_record_end)
		END DO

! 		Loop through the update times
		DO i_hr = 1,fl%update_times
! 			Write the date and time
			CALL csv_write (41,fl%iendat(i_hr),.FALSE.)
			CALL csv_write (41,fl%nentim(i_hr),.FALSE.)
! 			Calculate the output species vector
			out_conc = MATMUL(fl%conc(stn_x,stn_y,1,i_hr,:),conv_matrix)
! 			Write the output concentrations
			DO i_sp_o = 1,n_out_spec
				IF (i_sp_o .LT. n_out_spec) THEN
					csv_record_end=.FALSE.
				ELSE
					csv_record_end=.TRUE.
				END IF
				CALL csv_write (41,out_conc(i_sp_o),csv_record_end)
			END DO
		END DO

! 		Close the file
		CLOSE(41)
	END DO

END PROGRAM  after_extract_stat
