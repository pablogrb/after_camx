#!/bin/bash
stat=$1
head -1 1195-$stat.csv > base-$stat.csv
for file in [^base]*$stat.csv
do
	echo $file
	tail -n +2 -q $file >> base-$stat.csv
done
